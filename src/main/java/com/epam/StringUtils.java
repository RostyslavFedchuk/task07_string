package com.epam;

import java.util.Arrays;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class StringUtils {
    private static Logger logger = LogManager.getLogger(StringUtils.class);

    public static String concatAll(Object... objects) {
        Optional<String> stringUnion = Arrays.stream(objects)
                .map(Object::toString)
                .reduce(String::concat);
        return stringUnion.get();
    }

    public static void main(String[] args) {
//        logger.info(concatAll("git", " is cool"));
//        logger.info(isSentences("Pello my friend."));
        logger.info(replaceAllVowels("Hello my frined"));
    }

    private static boolean isSentences(String sentence){
        String regex = "[A-Z].+[.]";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(sentence);
        return matcher.matches();
    }

    private static String[] split(String sentence){
        return sentence.split("you|the");
    }

    private static String replaceAllVowels(String text){
        return text.replaceAll("[euioya]","_");
    }
}
