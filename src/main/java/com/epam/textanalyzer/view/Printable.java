package com.epam.textanalyzer.view;

public interface Printable {
    void print();
}
