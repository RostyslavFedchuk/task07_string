package com.epam.textanalyzer.view;

import com.epam.textanalyzer.Main;
import com.epam.textanalyzer.controller.Controller;
import com.epam.textanalyzer.controller.TextAnalyzer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class MyView {
    Controller analyzer;
    private static final int tasksCount = 20;
    private static Logger logger = LogManager.getLogger(Main.class);
    private static final Scanner SCANNER = new Scanner(System.in);

    private Map<String, String> menu;
    private Map<String, Printable> menuMethods;

    Locale locale;
    ResourceBundle bundle;

    public MyView() {
        analyzer = new TextAnalyzer();
        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("Menu", locale);
        setMenu();
        setMenuMethods();
    }

    private void setMenu() {
        menu = new LinkedHashMap<>();
        for (int i = 1; i <= tasksCount; i++) {
            menu.put("" + i, bundle.getString("" + i));
        }
        menu.put("Q", bundle.getString("Q"));
    }

    private void setMenuMethods() {
        menuMethods = new LinkedHashMap<>();
        menuMethods.put("1", this::printMaxTensesWithDuplicates);
        menuMethods.put("2", this::printSortedByWordsCount);
        menuMethods.put("3", this::printUniqueWordFromFirstSentence);
        menuMethods.put("4", this::printInterrogativeWords);
        menuMethods.put("5", this::swapSpecificWords);
        menuMethods.put("6", this::printWordsAlphabetically);
        menuMethods.put("7", this::sortByVowelPercentage);
        menuMethods.put("8", this::sortBySecondConsonant);
        menuMethods.put("9", this::sortAscByCharOccurrence);
        menuMethods.put("10", this::sortByWordOccurrence);
        menuMethods.put("11", this::deleteSubstringFromSentences);
        menuMethods.put("12", this::deleteWordsWithConsonantByLength);
        menuMethods.put("13", this::sortDescByCharOccurrence);
        menuMethods.put("14", this::findLongestPalindrome);
        menuMethods.put("15", this::deleteSymbolsByFirst);
        menuMethods.put("16", this::setWordByLengthInSpecificTense);
        menuMethods.put("17", this::printText);
        menuMethods.put("18", this::setMenuToEnglish);
        menuMethods.put("19", this::setMenuToGerman);
        menuMethods.put("20", this::setMenuToUkrainian);
        menu.put("Q", bundle.getString("Q"));
    }

    private void printMenu() {
        logger.info("\n______________________________________________"
                + "MENU______________________________________________\n");
        for (Map.Entry entry : menu.entrySet()) {
            logger.info(entry.getKey() + " - " + entry.getValue() + "\n");
        }
        logger.info("______________________________________________"
                + "__________________________________________________\n");
    }

    private void printText() {
        logger.info(analyzer.toString());
    }

    private void printMaxTensesWithDuplicates() {
        analyzer.printMaxTensesWithDuplicates();
    }

    private void printSortedByWordsCount() {
        analyzer.printSortedByWordsCount();
    }

    private void printUniqueWordFromFirstSentence() {
        analyzer.printUniqueWordFromFirstSentence();
    }

    private void printInterrogativeWords() {
        logger.info("Enter word`s length to find it in interrogative sentences: ");
        int length = SCANNER.nextInt();
        if (length < 0) {
            logger.info("Word`s length cannot be less than 1!!\n");
            return;
        }
        analyzer.printInterrogativeWords(length);
    }

    private void swapSpecificWords() {
        analyzer.swapSpecificWords();
    }

    private void printWordsAlphabetically() {
        analyzer.printWordsAlphabetically();
    }

    private void sortByVowelPercentage() {
        analyzer.sortByVowelPercentage();
    }

    private void sortBySecondConsonant() {
        analyzer.sortBySecondConsonant();
    }

    private void sortAscByCharOccurrence() {
        logger.info("Enter a char to print ascending sorted text by char`s occurrence: ");
        analyzer.sortAscByCharOccurrence(SCANNER.next().charAt(0));
    }

    private void sortByWordOccurrence() {
        analyzer.sortByWordOccurrence();
    }

    private void deleteSubstringFromSentences() {
        logger.info("Enter two chars without spaces: ");
        String chars = SCANNER.next();
        if (chars.length() >= 2) {
            analyzer.deleteSubstringFromSentences(chars.charAt(0), chars.charAt(1));
        } else {
            logger.info("You entered less than two chars!!\n");
        }
    }

    private void deleteWordsWithConsonantByLength() {
        logger.info("Enter the word`s length: ");
        int length = SCANNER.nextInt();
        if (length > 0) {
            analyzer.deleteWordsWithConsonantByLength(length);
        } else {
            logger.info("Word`s length cannot be less than 1!!\n");
        }
    }

    private void findLongestPalindrome() {
        analyzer.findLongestPalindrome();
    }

    private void sortDescByCharOccurrence() {
        logger.info("Enter a char to print descending sorted text by char`s occurrence:\n");
        analyzer.sortDescByCharOccurrence(SCANNER.next().charAt(0));
    }

    private void deleteSymbolsByFirst() {
        analyzer.deleteSymbolsByFirst();
    }

    private void setWordByLengthInSpecificTense() {
        logger.info("Enter index, length and a new substring to put it: ");
        int index = SCANNER.nextInt();
        int length = SCANNER.nextInt();
        String substring = SCANNER.nextLine();
        if (index < 0 || length < 0) {
            logger.info("Incorrect input!!\n");
        }
        analyzer.setWordByLengthInSpecificTense(index, length, substring);
    }

    private void setMenuToEnglish() {
        locale = new Locale("en");
        bundle = ResourceBundle.getBundle("Menu", locale);
        setMenu();
    }

    private void setMenuToGerman() {
        locale = new Locale("de");
        bundle = ResourceBundle.getBundle("Menu", locale);
        setMenu();
    }

    private void setMenuToUkrainian() {
        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("Menu", locale);
        setMenu();
    }

    public void showMenu() {
        String option;
        do {
            printMenu();
            logger.info("Choose one option: ");
            option = SCANNER.next().toUpperCase();
            if (menuMethods.containsKey(option)) {
                menuMethods.get(option).print();
            } else {
                logger.info("There is no option `" + option + "`. Try again!");
            }
        } while (!option.equals("Q"));
    }
}
