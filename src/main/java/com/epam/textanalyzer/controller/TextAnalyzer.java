package com.epam.textanalyzer.controller;

import com.epam.textanalyzer.model.Sentence;
import com.epam.textanalyzer.model.TextReaders;
import com.epam.textanalyzer.model.Word;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.text.BreakIterator;
import java.util.*;
import java.util.stream.Collectors;

public class TextAnalyzer implements Controller {
    private static Logger logger = LogManager.getLogger(TextAnalyzer.class);
    private static final int MIN_PALINDROME_LENGTH = 3;
    private StringBuilder text;
    private List<Sentence> sentences;

    public TextAnalyzer() {
        text = new StringBuilder(TextReaders.INSTANCE.getText());
        setSentences();
    }

    private void setSentences() {
        sentences = new LinkedList<>();
        BreakIterator iterator = BreakIterator.getSentenceInstance();
        iterator.setText(text.toString());
        int start = iterator.first();
        int end = iterator.next();
        while (end != BreakIterator.DONE) {
            sentences.add(new Sentence(text.substring(start, end).replaceAll("\\s{2,}", " ")));
            start = end;
            end = iterator.next();
        }
    }

    //2
    public void printSortedByWordsCount() {
        logger.info("Sorted sentences by words count:\n");
        sentences.stream()
                .sorted(Comparator.comparing(Sentence::countWords))
                .forEach((s) -> logger.info(s + " "));
    }

    //4
    public void printInterrogativeWords(int length) {
        for (Sentence tense : sentences) {
            if (tense.isInterrogative()) {
                tense.printWordsByLength(length);
            }
        }
    }

    //3
    public void printUniqueWordFromFirstSentence() {
        if (sentences.size() > 0) {
            logger.info("Unique words in first sentence that is never used in all others:\n");
            Word foundWord = sentences.get(0).getWords().stream()
                    .filter(word -> sentences.stream().skip(1)
                            .noneMatch(sentences -> sentences.contains(word)))
                    .findFirst().orElse(new Word(""));
            logger.info(foundWord + "\n");
        } else {
            logger.info("Sentences are empty!\n");
        }
    }

    //5
    public void swapSpecificWords() {
        logger.info("\nWe swapped the biggest word and the first word that begins with vowel.\n");
        sentences.forEach(Sentence::swapSpecificWords);
        logger.info(this::toString);
    }

    //6
    public void printWordsAlphabetically() {
        logger.info("\nAll words sorted alphabetically:\n");
        Optional<String> wordSream = sentences.stream()
                .flatMap(tense -> tense.getWords().stream())
                .map(Word::getWord)
                .sorted(String::compareToIgnoreCase)
                .reduce((a, b) -> {
                    if (a.substring(0, 1).compareToIgnoreCase(b.substring(0, 1)) == 0) {
                        a = a + " " + b + " ";
                        return a;
                    } else {
                        a += "\n";
                        logger.info(a);
                        return b;
                    }
                });
        wordSream.ifPresent(s -> logger.info(s));
    }

    //12
    public void deleteWordsWithConsonantByLength(int length) {
        logger.info("\nAll words that have first consonant letter and " +
                "length " + length + " were deleted!\n");
        sentences.forEach(s -> s.removeConsonantByLength(length));
        logger.info(this::toString);
    }

    //9
    public void sortAscByCharOccurrence(char symbol) {
        Comparator<Word> comparator = (a, b) -> {
            if (a.countChar(symbol) > b.countChar(symbol)) {
                return 1;
            } else if (a.countChar(symbol) == b.countChar(symbol)) {
                return a.getWord().compareToIgnoreCase(b.getWord());
            } else {
                return -1;
            }
        };
        logger.info("\nAscending sorted words by the " + symbol + " occurrence:\n");
        sentences.stream().flatMap(tense -> tense.getWords().stream())
                .sorted(comparator).forEach(s -> logger.info(s + " "));
    }

    //13
    public void sortDescByCharOccurrence(char symbol) {
        Comparator<Word> comparator = (a, b) -> {
            if (a.countChar(symbol) < b.countChar(symbol)) {
                return 1;
            } else if (a.countChar(symbol) == b.countChar(symbol)) {
                return a.getWord().compareToIgnoreCase(b.getWord());
            } else {
                return -1;
            }
        };
        logger.info("\nDescending sorted words by the " + symbol + " occurrence:\n");
        sentences.stream().flatMap(tense -> tense.getWords().stream())
                .sorted(comparator).forEach(s -> logger.info(s + " "));
    }

    //7
    public void sortByVowelPercentage() {
        logger.info("\nAscending sorted words by the vowel percentage in them:\n");
        sentences.stream().flatMap(tense -> tense.getWords().stream())
                .sorted(Comparator.comparing(Word::getVowelPercentage))
                .forEach(s -> logger.info(s + " "));
    }

    //8
    public void sortBySecondConsonant() {
        logger.info("\nWords that has first vowel sorted by first consonant occurrence:\n");
        sentences.stream().flatMap(tense -> tense.getWords().stream())
                .filter(Word::isFirstVowel)
                .sorted(Comparator.comparing(Word::getFirstConsonant))
                .forEach(s -> logger.info(s + " "));
    }

    //10
    public void sortByWordOccurrence() {
        sentences.forEach(Sentence::getWordOccurrence);
        logger.info("\nWords sorted by occurrence count in the whole text:\n");
        Map<String, Long> wordsOccurrence = sentences.stream()
                .flatMap(tense -> tense.getWords().stream())
                .collect(Collectors.groupingByConcurrent(Word::getWord, Collectors.counting()));

        wordsOccurrence.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .forEachOrdered(s -> logger.info(s + " "));
    }

    //15
    public void deleteSymbolsByFirst() {
        logger.info("\nIn every word were deleted all symbols that is the same to first char:\n");
        sentences.stream().flatMap(sentence -> sentence.getWords().stream())
                .forEach(Word::deleteCharsByFirst);
        logger.info(this::toString);
    }

    //16
    public void setWordByLengthInSpecificTense(int index, int length, String newWord) {
        if (index < sentences.size()) {
            logger.info("\nAll words with length " + length + " were updated to "
                    + newWord + " in " + (index + 1) + "-th sentence:\n");
            sentences.get(index).getWords().stream()
                    .filter(word -> word.toString().length() == length)
                    .forEach(word -> word.setWord(newWord));
            sentences.get(index).getWords().forEach(s -> logger.info(s + " "));
        } else {
            logger.info("There is less than " + index + " sentences");
        }
    }

    //1
    public void printMaxTensesWithDuplicates() {
        Map<Sentence, Integer> countSameTenses = new LinkedHashMap<>();
        for (int i = 0; i < sentences.size(); i++) {
            int countSame = 1;
            countSameTenses.put(sentences.get(i), countSame);
            for (int j = 0; j < sentences.size(); j++) {
                if (i != j) {
                    int finalJ = j;
                    Word foundSame = sentences.get(i).getWords().stream()
                            .filter(word -> sentences.get(finalJ).getWords()
                                    .stream().anyMatch(w -> w.equals(word)))
                            .findFirst().orElse(new Word(""));
                    if (!foundSame.toString().isEmpty()) {
                        countSameTenses.put(sentences.get(i), ++countSame);
                    }
                }
            }
        }
        Integer maxCount = countSameTenses.entrySet().stream()
                .max(Comparator.comparing(Map.Entry::getValue)).get().getValue();
        logger.info("\nMax count of sentences that have same words is: " + maxCount + "\n");
    }

    //11
    public void deleteSubstringFromSentences(char begin, char end) {
        sentences.stream().forEach(sentence -> sentence.findAllSubstrings(begin, end));
        logger.info(this::toString);
    }

    //14
    public void findLongestPalindrome() {
        List<String> palindromes = new ArrayList<>();
        String text = this.text.toString().toLowerCase();
        for (int i = 0; i < text.length(); i++) {
            if (text.length() > i + MIN_PALINDROME_LENGTH) {
                for (int j = i + MIN_PALINDROME_LENGTH; j < text.length(); j++) {
                    if (text.charAt(i) == text.charAt(j)) {
                        String substring = text.substring(i, j + 1).replaceAll("[.,!?:;]", "");
                        String substrModified = substring.replaceAll("[\\s]", "");
                        StringBuilder reverse = new StringBuilder(substrModified).reverse();
                        if (substrModified.equals(reverse.toString())) {
                            palindromes.add(substring);
                        }
                    }
                }
            }
        }
        showLongest(palindromes);
    }

    public void showLongest(List<String> palindromes) {
        Optional<String> longest = palindromes.stream().max(Comparator.comparing(String::length));
        if (longest.isPresent()) {
            logger.info("\nThe longest Palindrome in the text is: \n");
            logger.info(longest.get());
        } else {
            logger.info("There is no palindromes in the text...");
        }
    }

    @Override
    public String toString() {
        StringBuilder show = new StringBuilder("This is your text:\n");
        sentences.stream().forEach(s -> show.append(s + "\n"));
        return show.toString();
    }
}
