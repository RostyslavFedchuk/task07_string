package com.epam.textanalyzer.controller;

public interface Controller {
    void printMaxTensesWithDuplicates();

    void printSortedByWordsCount();

    void printUniqueWordFromFirstSentence();

    void printInterrogativeWords(int length);

    void swapSpecificWords();

    void printWordsAlphabetically();

    void sortByVowelPercentage();

    void sortBySecondConsonant();

    void sortAscByCharOccurrence(char symbol);

    void sortByWordOccurrence();

    void deleteSubstringFromSentences(char begin, char end);

    void deleteWordsWithConsonantByLength(int length);

    void findLongestPalindrome();

    void sortDescByCharOccurrence(char symbol);

    void deleteSymbolsByFirst();

    void setWordByLengthInSpecificTense(int index, int length, String substring);

}
