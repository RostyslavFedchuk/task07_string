package com.epam.textanalyzer.model;

import static java.util.Objects.isNull;

public class Punctuation {
    private String punctuation;

    public Punctuation(String punctuation) {
        this.punctuation = punctuation;
    }

    public String getPunctuation(){
        return punctuation;
    }

    @Override
    public boolean equals(Object obj) {
        if(isNull(obj)){
            return false;
        }
        if(obj == this){
            return true;
        }
        if(!(obj instanceof Punctuation)){
            return false;
        } else {
            return ((Punctuation) obj).getPunctuation().equals(getPunctuation());
        }
    }

    @Override
    public String toString() {
        return punctuation;
    }
}
