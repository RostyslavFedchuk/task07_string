package com.epam.textanalyzer.model;

import java.io.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static java.util.Objects.*;

public enum TextReaders {
    INSTANCE;

    private static StringBuilder text;

    private static Logger logger = LogManager.getLogger(TextReaders.class);

    static {
        text = new StringBuilder();
        readFromFile();
    }

    public static void readFromFile(){
        try(InputStream input = TextReaders.class.getClassLoader().getResourceAsStream("text.txt")){
            BufferedReader reader = new BufferedReader(new InputStreamReader(input));
            String line;
            while (nonNull(line = reader.readLine())){
                text.append(line);
            }
            text = new StringBuilder(text.toString().replaceAll("\\s{2,}"," "));
        } catch (FileNotFoundException e){
            logger.warn("File was not found!");
        } catch (IOException e) {
            logger.warn("Problems with openning file");
        }
    }

    public String getText() {
        return text.toString();
    }
}
