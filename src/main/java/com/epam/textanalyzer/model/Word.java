package com.epam.textanalyzer.model;

import java.util.stream.Stream;

import static java.util.Objects.isNull;

public class Word {
    private StringBuilder word;

    public Word(String word) {
        this.word = new StringBuilder(word);
    }

    public String getWord() {
        return word.toString();
    }

    public void setWord(String word) {
        this.word = new StringBuilder(word);
    }

    public boolean isFirstConsonant() {
        return word.substring(0, 1).matches("[^eyuioaEYUIOA]");
    }

    public boolean isFirstVowel() {
        return word.substring(0, 1).matches("[eyuioaEYUIOA]");
    }

    public int countChar(char symbol) {
        int countChar = 0;
        for (int i = 0; i < word.length(); i++) {
            if (word.toString().toLowerCase().charAt(i) == symbol) {
                countChar++;
            }
        }
        return countChar;
    }

    public double getVowelPercentage() {
        return (double) Stream.of(word).flatMap(word -> Stream.of(word.toString().split("")))
                .filter(s -> s.toString().matches("[eyuioaEYUIOA]"))
                .count() / word.length();
    }

    public String getFirstConsonant() {
        for (String letter : word.toString().split("")) {
            if (letter.matches("[^euioaEUIOA]")) {
                return letter;
            }
        }
        return "";
    }

    public void deleteCharsByFirst() {
        if (word.length() > 1) {
            StringBuilder newWord = new StringBuilder("" + word.charAt(0));
            char firstChar = word.toString().toLowerCase().charAt(0);
            for (int i = 1; i < word.length(); i++) {
                if (word.toString().toLowerCase().charAt(i) != firstChar) {
                    newWord.append(word.charAt(i));
                }
            }
            word = new StringBuilder(newWord);
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (isNull(obj)) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Word)) {
            return false;
        } else {
            return ((Word) obj).getWord().equals(getWord());
        }
    }

    @Override
    public String toString() {
        return word.toString();
    }
}
