package com.epam.textanalyzer.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.text.BreakIterator;
import java.util.*;
import java.util.stream.Collectors;

public class Sentence {
    private static Logger logger = LogManager.getLogger(Sentence.class);
    private StringBuilder sentence;
    private List<Word> words;
    private List<Punctuation> punctuations;

    public Sentence(String sentence) {
        this.sentence = new StringBuilder(sentence);
        words = new LinkedList<>();
        punctuations = new LinkedList<>();
        setWords();
    }

    public int countWords() {
        return words.size();
    }

    private void setWords() {
        BreakIterator iterator = BreakIterator.getWordInstance();
        iterator.setText(sentence.toString());
        int start = iterator.first();
        int end = iterator.next();
        while (end != BreakIterator.DONE) {
            String word = sentence.substring(start, end);
            if (!word.matches("\\s+")) {
                if (word.matches("\\W")) {
                    punctuations.add(new Punctuation(word));
                } else {
                    words.add(new Word(word));
                }
            }
            start = end;
            end = iterator.next();
        }
    }

    public boolean isInterrogative() {
        return punctuations.contains(new Punctuation("?"));
    }

    public boolean contains(Word word) {
        return words.contains(word);
    }

    public void printWordsByLength(int length) {
        logger.info("\nAll words that have length " + length + " in one tense:\n");
        words.stream()
                .map(Word::toString)
                .filter(s -> s.length() == length)
                .distinct()
                .forEach(s -> logger.info(s + " "));
    }

    public void swapSpecificWords() {
        Word biggestWord = getBiggestWord();
        int indexBiggestWord = words.indexOf(biggestWord);
        Word firstVowelWord = getWordWithFirstVowel();
        int indexFirstVowelWord = words.indexOf(firstVowelWord);
        if (!firstVowelWord.toString().isEmpty()) {
            words.set(indexBiggestWord, firstVowelWord);
            words.set(indexFirstVowelWord, biggestWord);
        }
    }

    public void removeConsonantByLength(int length) {
        for (int i = 0; i < words.size(); i++) {
            if (words.get(i).getWord().length() == length && words.get(i).isFirstConsonant()) {
                words.remove(i);
            }
        }
        setSentence();
    }

    public void getWordOccurrence() {
        logger.info("\nOccurrence word count in the sentence:\n");
        Map<String, Long> wordsOccurrence = words.stream()
                .collect(Collectors.groupingByConcurrent(Word::getWord, Collectors.counting()));
        wordsOccurrence.entrySet().forEach(s -> logger.info(s + " "));
    }

    public void findAllSubstrings(char begin, char end) {
        List<String> substring = new ArrayList<>();
        String tense = sentence.toString().toLowerCase();
        for (int i = 0; i < tense.length(); i++) {
            if (tense.charAt(i) == begin) {
                for (int j = i; j < tense.length(); j++) {
                    if (tense.charAt(j) == end) {
                        substring.add(sentence.substring(i, j + 1));
                    }
                }
            }
        }
        Optional<String> optionalS = substring.stream().max(Comparator.comparing(String::length));
        if (optionalS.isPresent()) {
            deleteSubstring(optionalS.get());
        }
    }

    public void deleteSubstring(String substring) {
        sentence = new StringBuilder(sentence.toString().replaceAll(substring, ""));
        words.clear();
        setWords();
    }

    public Word getBiggestWord() {
        return getWords().stream()
                .max(Comparator.comparing(word -> word.toString().length()))
                .orElse(new Word(""));
    }

    public Word getWordWithFirstVowel() {
        return getWords().stream()
                .filter(Word::isFirstVowel)
                .findFirst().orElse(new Word(""));
    }

    public List<Word> getWords() {
        return words;
    }

    public void setSentence() {
        StringBuilder tense = new StringBuilder();
        for (Word word : words) {
            tense.append(word.getWord() + " ");
        }
        sentence = new StringBuilder(tense);
    }

    @Override
    public String toString() {
        setSentence();
        return sentence.toString();
    }

}
